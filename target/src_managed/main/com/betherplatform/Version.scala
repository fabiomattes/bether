package com.betherplatform

object Version {
  val VersionString = "0.16.2-372-g7561600-DIRTY"
  val VersionTuple = (0, 16, 2)
}
