package com.betherplatform.generator.utils

import com.betherplatform.account.PrivateKeyAccount
import com.betherplatform.common.state.ByteStr

object Universe {
  var AccountsWithBalances: List[(PrivateKeyAccount, Long)] = Nil
  var IssuedAssets: List[ByteStr]                           = Nil
  var Leases: List[ByteStr]                                 = Nil
}
