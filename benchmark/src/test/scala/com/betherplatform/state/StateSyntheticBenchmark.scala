package com.betherplatform.state

import java.util.concurrent.TimeUnit

import com.betherplatform.account.PrivateKeyAccount
import com.betherplatform.common.utils.EitherExt2
import com.betherplatform.lang.StdLibVersion.V1
import com.betherplatform.lang.v1.compiler.ExpressionCompiler
import com.betherplatform.lang.v1.parser.Parser
import com.betherplatform.settings.FunctionalitySettings
import com.betherplatform.state.StateSyntheticBenchmark._
import com.betherplatform.transaction.Asset.Bether
import com.betherplatform.transaction.Transaction
import com.betherplatform.transaction.smart.SetScriptTransaction
import com.betherplatform.transaction.smart.script.v1.ExprScript
import com.betherplatform.transaction.transfer._
import com.betherplatform.utils.compilerContext
import org.openjdk.jmh.annotations._
import org.scalacheck.Gen

@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Array(Mode.AverageTime))
@Threads(1)
@Fork(1)
@Warmup(iterations = 10)
@Measurement(iterations = 10)
class StateSyntheticBenchmark {

  @Benchmark
  def appendBlock_test(db: St): Unit = db.genAndApplyNextBlock()

  @Benchmark
  def appendBlock_smart_test(db: SmartSt): Unit = db.genAndApplyNextBlock()

}

object StateSyntheticBenchmark {

  @State(Scope.Benchmark)
  class St extends BaseState {
    protected override def txGenP(sender: PrivateKeyAccount, ts: Long): Gen[Transaction] =
      for {
        amount    <- Gen.choose(1, bether(1))
        recipient <- accountGen
      } yield TransferTransactionV1.selfSigned(Bether, sender, recipient, amount, ts, Bether, 100000, Array.emptyByteArray).explicitGet()
  }

  @State(Scope.Benchmark)
  class SmartSt extends BaseState {

    override protected def updateFunctionalitySettings(base: FunctionalitySettings): FunctionalitySettings = {
      base.copy(preActivatedFeatures = Map(4.toShort -> 0))
    }

    protected override def txGenP(sender: PrivateKeyAccount, ts: Long): Gen[Transaction] =
      for {
        recipient: PrivateKeyAccount <- accountGen
        amount                       <- Gen.choose(1, bether(1))
      } yield
        TransferTransactionV2
          .selfSigned(
            Bether,
            sender,
            recipient.toAddress,
            amount,
            ts,
            Bether,
            1000000,
            Array.emptyByteArray
          )
          .explicitGet()

    @Setup
    override def init(): Unit = {
      super.init()

      val textScript    = "sigVerify(tx.bodyBytes,tx.proofs[0],tx.senderPublicKey)"
      val untypedScript = Parser.parseExpr(textScript).get.value
      val typedScript   = ExpressionCompiler(compilerContext(V1, isAssetScript = false), untypedScript).explicitGet()._1

      val setScriptBlock = nextBlock(
        Seq(
          SetScriptTransaction
            .selfSigned(
              richAccount,
              Some(ExprScript(typedScript).explicitGet()),
              1000000,
              System.currentTimeMillis()
            )
            .explicitGet()
        )
      )

      applyBlock(setScriptBlock)
    }
  }

}
