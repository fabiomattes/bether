package com.betherplatform.lang.utils
import com.betherplatform.lang.ContentType.ContentType
import com.betherplatform.lang.ScriptType.ScriptType
import com.betherplatform.lang.StdLibVersion.StdLibVersion

case class DirectiveSet(stdLibVersion: StdLibVersion, scriptType: ScriptType, contentType: ContentType)