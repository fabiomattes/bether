package com.betherplatform.lang.v1.traits.domain

import com.betherplatform.common.state.ByteStr

case class APair(amountAsset: Option[ByteStr], priceAsset: Option[ByteStr])
