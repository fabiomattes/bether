package com.betherplatform.lang.directives

final case class Directive(key: DirectiveKey, value: String)
