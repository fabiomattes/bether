package com.betherplatform.lang

import cats.kernel.Monoid
import com.betherplatform.lang.StdLibVersion.V2
import com.betherplatform.lang.v1.compiler.ExpressionCompiler
import com.betherplatform.lang.v1.compiler.Terms.EXPR
import com.betherplatform.lang.v1.evaluator.ctx.impl.bether.BetherContext
import com.betherplatform.lang.v1.evaluator.ctx.impl.{CryptoContext, PureContext}

object JavaAdapter {
  private val version = V2

  lazy val ctx =
    Monoid.combineAll(
      Seq(
        CryptoContext.compilerContext(com.betherplatform.lang.Global),
        BetherContext.build(???, null).compilerContext,
        PureContext.build(version).compilerContext
      ))

  def compile(input: String): EXPR = {
    ExpressionCompiler
      .compile(input, ctx)
      .fold(
        error => throw new IllegalArgumentException(error),
        expr => expr
      )
  }
}
