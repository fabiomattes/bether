package com.betherplatform.http

import com.betherplatform.{NTPTime, TestWallet}
import com.betherplatform.settings.BetherSettings
import com.betherplatform.api.http.ApiKeyNotValid

class DebugApiRouteSpec extends RouteSpec("/debug") with RestAPISettingsHelper with TestWallet with NTPTime {
  private val sampleConfig   = com.typesafe.config.ConfigFactory.load()
  private val betherSettings = BetherSettings.fromConfig(sampleConfig)
  private val configObject   = sampleConfig.root()
  private val route =
    DebugApiRoute(betherSettings, ntpTime, null, null, null, null, null, null, null, null, null, null, null, null, null, configObject).route

  routePath("/configInfo") - {
    "requires api-key header" in {
      Get(routePath("/configInfo?full=true")) ~> route should produce(ApiKeyNotValid)
      Get(routePath("/configInfo?full=false")) ~> route should produce(ApiKeyNotValid)
    }
  }
}
