package com.betherplatform.settings

import java.io.File

import com.typesafe.config.ConfigFactory
import org.scalatest.{FlatSpec, Matchers}

class BetherSettingsSpecification extends FlatSpec with Matchers {

  val expectedDir =
    ConfigFactory
      .defaultOverrides()
      .withFallback(ConfigFactory.parseString(s"bether.directory=${com.betherplatform.settings.defaultDirectory}"))
      .resolve()
      .getString("bether.directory")

  private def config(configName: String) = {
    BetherSettings.fromConfig(
      com.betherplatform.settings.loadConfig(
        ConfigFactory.parseFile(new File(s"bether-$configName.conf"))
      )
    )
  }

  def testConfig(configName: String)(additionalChecks: BetherSettings => Unit = _ => ()) {
    "BetherSettings" should s"read values from default config with $configName overrides" in {
      val settings = config(configName)

      settings.directory should be(expectedDir)
      settings.networkSettings should not be null
      settings.walletSettings should not be null
      settings.blockchainSettings should not be null
      settings.matcherSettings should not be null
      settings.minerSettings should not be null
      settings.restAPISettings should not be null
      settings.synchronizationSettings should not be null
      settings.utxSettings should not be null
      additionalChecks(settings)
    }
  }

  testConfig("mainnet")()
  testConfig("testnet")()
  testConfig("devnet")()

  "BetherSettings" should "resolve folders correctly" in {
    val config = loadConfig(ConfigFactory.parseString(s"""bether {
         |  directory = "/xxx"
         |  data-directory = "/xxx/data"
         |  ntp-server = "example.com"
         |}""".stripMargin))

    val settings = BetherSettings.fromConfig(config.resolve())

    settings.directory should be("/xxx")
    settings.dataDirectory should be("/xxx/data")
    settings.ntpServer should be("example.com")
    settings.networkSettings.file should be(Some(new File("/xxx/peers.dat")))
    settings.walletSettings.file should be(Some(new File("/xxx/wallet/wallet.dat")))
    settings.matcherSettings.journalDataDir should be("/xxx/matcher/journal")
    settings.matcherSettings.snapshotsDataDir should be("/xxx/matcher/snapshots")
  }

}
