package com.betherplatform.db
import com.typesafe.config.ConfigFactory
import com.betherplatform.settings.BetherSettings

trait DBCacheSettings {
  lazy val maxCacheSize: Int = {
    val settings = BetherSettings.fromConfig(ConfigFactory.load())
    settings.maxCacheSize
  }
}
