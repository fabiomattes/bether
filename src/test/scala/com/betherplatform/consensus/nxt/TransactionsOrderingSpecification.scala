package com.betherplatform.consensus.nxt

import com.betherplatform.account.{Address, PrivateKeyAccount}
import com.betherplatform.common.state.ByteStr
import com.betherplatform.common.utils.EitherExt2
import com.betherplatform.consensus.TransactionsOrdering
import com.betherplatform.transaction.Asset
import com.betherplatform.transaction.Asset.Bether
import com.betherplatform.transaction.transfer._
import org.scalatest.{Assertions, Matchers, PropSpec}

import scala.util.Random

class TransactionsOrderingSpecification extends PropSpec with Assertions with Matchers {

  property("TransactionsOrdering.InBlock should sort correctly") {
    val correctSeq = Seq(
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    1,
                    Bether,
                    125L,
                    Array.empty)
        .right
        .get,
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    2,
                    Bether,
                    124L,
                    Array.empty)
        .right
        .get,
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    1,
                    Bether,
                    124L,
                    Array.empty)
        .right
        .get,
      TransferTransactionV1
        .selfSigned(
          Bether,
          PrivateKeyAccount(Array.fill(32)(0)),
          Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
          100000,
          2,
          Asset.fromCompatId(Some(ByteStr.empty)),
          124L,
          Array.empty
        )
        .right
        .get,
      TransferTransactionV1
        .selfSigned(
          Bether,
          PrivateKeyAccount(Array.fill(32)(0)),
          Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
          100000,
          1,
          Asset.fromCompatId(Some(ByteStr.empty)),
          124L,
          Array.empty
        )
        .right
        .get
    )

    val sorted = Random.shuffle(correctSeq).sorted(TransactionsOrdering.InBlock)

    sorted shouldBe correctSeq
  }

  property("TransactionsOrdering.InUTXPool should sort correctly") {
    val correctSeq = Seq(
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    1,
                    Bether,
                    124L,
                    Array.empty)
        .right
        .get,
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    1,
                    Bether,
                    123L,
                    Array.empty)
        .right
        .get,
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    2,
                    Bether,
                    123L,
                    Array.empty)
        .right
        .get,
      TransferTransactionV1
        .selfSigned(
          Bether,
          PrivateKeyAccount(Array.fill(32)(0)),
          Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
          100000,
          1,
          Asset.fromCompatId(Some(ByteStr.empty)),
          124L,
          Array.empty
        )
        .right
        .get,
      TransferTransactionV1
        .selfSigned(
          Bether,
          PrivateKeyAccount(Array.fill(32)(0)),
          Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
          100000,
          2,
          Asset.fromCompatId(Some(ByteStr.empty)),
          124L,
          Array.empty
        )
        .right
        .get
    )

    val sorted = Random.shuffle(correctSeq).sorted(TransactionsOrdering.InUTXPool)

    sorted shouldBe correctSeq
  }

  property("TransactionsOrdering.InBlock should sort txs by decreasing block timestamp") {
    val correctSeq = Seq(
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    124L,
                    Bether,
                    1,
                    Array())
        .right
        .get,
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    123L,
                    Bether,
                    1,
                    Array())
        .right
        .get
    )

    Random.shuffle(correctSeq).sorted(TransactionsOrdering.InBlock) shouldBe correctSeq
  }

  property("TransactionsOrdering.InUTXPool should sort txs by ascending block timestamp") {
    val correctSeq = Seq(
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    123L,
                    Bether,
                    1,
                    Array())
        .right
        .get,
      TransferTransactionV1
        .selfSigned(Bether,
                    PrivateKeyAccount(Array.fill(32)(0)),
                    Address.fromString("3MydsP4UeQdGwBq7yDbMvf9MzfB2pxFoUKU").explicitGet(),
                    100000,
                    124L,
                    Bether,
                    1,
                    Array())
        .right
        .get
    )
    Random.shuffle(correctSeq).sorted(TransactionsOrdering.InUTXPool) shouldBe correctSeq
  }
}
