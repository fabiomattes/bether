package com.betherplatform

trait Shutdownable {
  def shutdown(): Unit
}
