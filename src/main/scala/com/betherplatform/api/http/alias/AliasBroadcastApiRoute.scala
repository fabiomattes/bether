package com.betherplatform.api.http.alias

import akka.http.scaladsl.server.Route
import com.betherplatform.api.http._
import com.betherplatform.http.BroadcastRoute
import com.betherplatform.settings.RestAPISettings
import com.betherplatform.utx.UtxPool
import io.netty.channel.group.ChannelGroup

case class AliasBroadcastApiRoute(settings: RestAPISettings, utx: UtxPool, allChannels: ChannelGroup) extends ApiRoute with BroadcastRoute {
  override val route = pathPrefix("alias" / "broadcast") {
    signedCreate
  }

  def signedCreate: Route = (path("create") & post) {
    json[SignedCreateAliasV1Request] { aliasReq =>
      doBroadcast(aliasReq.toTx)
    }
  }
}
