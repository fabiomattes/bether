package com.betherplatform.settings

import java.io.File

import com.betherplatform.common.state.ByteStr

case class WalletSettings(file: Option[File], password: Option[String], seed: Option[ByteStr])
