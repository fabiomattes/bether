package com.betherplatform.settings

import com.betherplatform.Version
import com.betherplatform.utils.ScorexLogging

/**
  * System constants here.
  */
object Constants extends ScorexLogging {
  val ApplicationName = "bether"
  val AgentName       = s"Bether v${Version.VersionString}"

  val UnitsInWave = 100000000L
  val TotalBether = 100000000L
}
