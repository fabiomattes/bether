package com.betherplatform.settings

case class FeaturesSettings(autoShutdownOnUnsupportedFeature: Boolean, supported: List[Short])
