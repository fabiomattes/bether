package com.betherplatform.transaction

import com.betherplatform.account.Address

case class AssetAcc(account: Address, assetId: Option[Asset])
