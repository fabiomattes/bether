package com.betherplatform.transaction

import com.betherplatform.account.PublicKeyAccount

trait Authorized {
  val sender: PublicKeyAccount
}
