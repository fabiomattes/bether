package com.betherplatform

import com.betherplatform.block.{Block, MicroBlock}
import com.betherplatform.utils.base58Length

package object transaction {
  val AssetIdLength: Int       = com.betherplatform.crypto.DigestSize
  val AssetIdStringLength: Int = base58Length(AssetIdLength)
  type DiscardedTransactions = Seq[Transaction]
  type DiscardedBlocks       = Seq[Block]
  type DiscardedMicroBlocks  = Seq[MicroBlock]
  type AuthorizedTransaction = Authorized with Transaction
}
