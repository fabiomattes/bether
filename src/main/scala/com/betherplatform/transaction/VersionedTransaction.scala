package com.betherplatform.transaction

trait VersionedTransaction {
  def version: Byte
}
