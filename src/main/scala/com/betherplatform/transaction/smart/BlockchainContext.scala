package com.betherplatform.transaction.smart

import cats.kernel.Monoid
import com.betherplatform.account.Address
import com.betherplatform.lang.{ContentType, Global, ScriptType}
import com.betherplatform.lang.StdLibVersion._
import com.betherplatform.lang.utils.DirectiveSet
import com.betherplatform.lang.v1.evaluator.ctx.EvaluationContext
import com.betherplatform.lang.v1.evaluator.ctx.impl.bether.BetherContext
import com.betherplatform.lang.v1.evaluator.ctx.impl.{CryptoContext, PureContext}
import com.betherplatform.state._
import monix.eval.Coeval

object BlockchainContext {

  type In = BetherEnvironment.In
  def build(version: StdLibVersion,
            nByte: Byte,
            in: Coeval[In],
            h: Coeval[Int],
            blockchain: Blockchain,
            isTokenContext: Boolean,
            isContract: Boolean,
            tthis: Coeval[Address]): EvaluationContext = {
    Monoid
      .combineAll(
        Seq(
          PureContext.build(version),
          CryptoContext.build(Global),
          BetherContext.build(DirectiveSet(version, ScriptType.isTokenScript(isTokenContext), ContentType.isContract(isContract)),
                              new BetherEnvironment(nByte, in, h, blockchain, tthis))
        ))
      .evaluationContext
  }

}
