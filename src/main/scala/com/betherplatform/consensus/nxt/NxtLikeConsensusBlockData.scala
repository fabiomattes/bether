package com.betherplatform.consensus.nxt
import com.betherplatform.common.state.ByteStr

case class NxtLikeConsensusBlockData(baseTarget: Long, generationSignature: ByteStr)
