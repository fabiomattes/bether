package com.betherplatform.consensus

import com.betherplatform.transaction.Asset.Bether
import com.betherplatform.transaction.Transaction

object TransactionsOrdering {
  trait BetherOrdering extends Ordering[Transaction] {
    def txTimestampOrder(ts: Long): Long
    private def orderBy(t: Transaction): (Double, Long, Long) = {
      val size        = t.bytes().size
      val byFee       = if (t.assetFee._1 != Bether) 0 else -t.assetFee._2
      val byTimestamp = txTimestampOrder(t.timestamp)

      (byFee.toDouble / size.toDouble, byFee, byTimestamp)
    }
    override def compare(first: Transaction, second: Transaction): Int = {
      implicitly[Ordering[(Double, Long, Long)]].compare(orderBy(first), orderBy(second))
    }
  }

  object InBlock extends BetherOrdering {
    // sorting from network start
    override def txTimestampOrder(ts: Long): Long = -ts
  }

  object InUTXPool extends BetherOrdering {
    override def txTimestampOrder(ts: Long): Long = ts
  }
}
