package com.betherplatform.utx

import com.betherplatform.account.Address
import com.betherplatform.common.state.ByteStr
import com.betherplatform.mining.MultiDimensionalMiningConstraint
import com.betherplatform.state.{Diff, Portfolio}
import com.betherplatform.transaction._

trait UtxPool extends AutoCloseable {
  self =>

  def putIfNew(tx: Transaction): Either[ValidationError, (Boolean, Diff)]

  def removeAll(txs: Traversable[Transaction]): Unit

  def spendableBalance(addr: Address, assetId: Asset): Long

  def pessimisticPortfolio(addr: Address): Portfolio

  def all: Seq[Transaction]

  def size: Int

  def transactionById(transactionId: ByteStr): Option[Transaction]

  def packUnconfirmed(rest: MultiDimensionalMiningConstraint): (Seq[Transaction], MultiDimensionalMiningConstraint)

}
