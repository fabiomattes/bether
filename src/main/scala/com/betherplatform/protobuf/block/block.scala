package com.betherplatform.protobuf

package object block {
  type PBBlock = com.betherplatform.protobuf.block.Block
  val PBBlock = com.betherplatform.protobuf.block.Block

  type VanillaBlock = com.betherplatform.block.Block
  val VanillaBlock = com.betherplatform.block.Block
}
