package com.betherplatform.protobuf

package object transaction {
  type PBOrder = com.betherplatform.protobuf.transaction.ExchangeTransactionData.Order
  val PBOrder = com.betherplatform.protobuf.transaction.ExchangeTransactionData.Order

  type VanillaOrder = com.betherplatform.transaction.assets.exchange.Order
  val VanillaOrder = com.betherplatform.transaction.assets.exchange.Order

  type PBTransaction = com.betherplatform.protobuf.transaction.Transaction
  val PBTransaction = com.betherplatform.protobuf.transaction.Transaction

  type PBSignedTransaction = com.betherplatform.protobuf.transaction.SignedTransaction
  val PBSignedTransaction = com.betherplatform.protobuf.transaction.SignedTransaction

  type VanillaTransaction = com.betherplatform.transaction.Transaction
  val VanillaTransaction = com.betherplatform.transaction.Transaction

  type VanillaSignedTransaction = com.betherplatform.transaction.SignedTransaction

  type VanillaAssetId = com.betherplatform.transaction.Asset
}
