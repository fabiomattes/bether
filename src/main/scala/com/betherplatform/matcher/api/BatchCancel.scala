package com.betherplatform.matcher.api
import com.betherplatform.account.Address
import com.betherplatform.transaction.assets.exchange.AssetPair

case class BatchCancel(address: Address, assetPair: Option[AssetPair], timestamp: Long)
