package com.betherplatform.state

import com.betherplatform.block.Block.BlockId
import com.betherplatform.consensus.nxt.NxtLikeConsensusBlockData

case class BlockMinerInfo(consensus: NxtLikeConsensusBlockData, timestamp: Long, blockId: BlockId)
