#!/bin/bash

trap 'kill -TERM $PID' TERM INT
echo Options: $BETHER_OPTS
java $BETHER_OPTS -jar /opt/bether/bether.jar /opt/bether/template.conf &
PID=$!
wait $PID
trap - TERM INT
wait $PID
EXIT_STATUS=$?
