package com.betherplatform.it

import com.typesafe.config.ConfigFactory.{defaultApplication, defaultReference}
import com.betherplatform.account.PublicKeyAccount
import com.betherplatform.block.Block
import com.betherplatform.common.state.ByteStr
import com.betherplatform.common.utils.EitherExt2
import com.betherplatform.consensus.PoSSelector
import com.betherplatform.db.openDB
import com.betherplatform.history.StorageFactory
import com.betherplatform.settings._
import com.betherplatform.transaction.Asset.Bether
import com.betherplatform.utils.NTP
import monix.execution.UncaughtExceptionReporter
import monix.reactive.Observer
import net.ceedubs.ficus.Ficus._

object BaseTargetChecker {
  def main(args: Array[String]): Unit = {
    val sharedConfig = Docker.genesisOverride
      .withFallback(Docker.configTemplate)
      .withFallback(defaultApplication())
      .withFallback(defaultReference())
      .resolve()
    val settings         = BetherSettings.fromConfig(sharedConfig)
    val genesisBlock     = Block.genesis(settings.blockchainSettings.genesisSettings).explicitGet()
    val db               = openDB("/tmp/tmp-db")
    val time             = new NTP("ntp.pool.org")
    val portfolioChanges = Observer.empty(UncaughtExceptionReporter.LogExceptionsToStandardErr)
    val bu               = StorageFactory(settings, db, time, portfolioChanges)
    val pos              = new PoSSelector(bu, settings.blockchainSettings, settings.synchronizationSettings)
    bu.processBlock(genesisBlock)

    try {
      NodeConfigs.Default.map(_.withFallback(sharedConfig)).collect {
        case cfg if cfg.as[Boolean]("bether.miner.enable") =>
          val account   = PublicKeyAccount(cfg.as[ByteStr]("public-key").arr)
          val address   = account.toAddress
          val balance   = bu.balance(address, Bether)
          val consensus = genesisBlock.consensusData
          val timeDelay = pos
            .getValidBlockDelay(bu.height, account.publicKey, consensus.baseTarget, balance)
            .explicitGet()

          f"$address: ${timeDelay * 1e-3}%10.3f s"
      }
    } finally time.close()
  }
}
