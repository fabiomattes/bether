package com.betherplatform.it.matcher

import com.betherplatform.account.PrivateKeyAccount
import com.betherplatform.it.api.{MatcherStatusResponse, OrderBookResponse, OrderbookHistory}
import com.betherplatform.matcher.queue.QueueEventWithMeta
import com.betherplatform.transaction.assets.exchange.AssetPair

case class MatcherState(offset: QueueEventWithMeta.Offset,
                        snapshots: Map[String, QueueEventWithMeta.Offset],
                        orderBooks: Map[AssetPair, OrderBookResponse],
                        orderStatuses: Map[String, MatcherStatusResponse],
                        reservedBalances: Map[PrivateKeyAccount, Map[String, Long]],
                        orderHistory: Map[PrivateKeyAccount, Map[AssetPair, Seq[OrderbookHistory]]])
