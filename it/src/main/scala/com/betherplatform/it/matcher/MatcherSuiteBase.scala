package com.betherplatform.it.matcher

import com.typesafe.config.Config
import com.betherplatform.it._
import com.betherplatform.it.transactions.NodesFromDocker
import org.scalatest._
import com.betherplatform.it.util._
import scala.concurrent.ExecutionContext

abstract class MatcherSuiteBase
    extends FreeSpec
    with Matchers
    with CancelAfterFailure
    with ReportingTestName
    with NodesFromDocker
    with BeforeAndAfterAll
    with MatcherNode {

  protected implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  val defaultAssetQuantity = 999999999999L

  val smartFee         = 0.004.bether
  val minFee           = 0.001.bether + smartFee
  val issueFee         = 1.bether
  val smartIssueFee    = 1.bether + smartFee
  val leasingFee       = 0.002.bether + smartFee
  val tradeFee         = 0.003.bether
  val smartTradeFee    = tradeFee + smartFee
  val twoSmartTradeFee = tradeFee + 2 * smartFee

  protected def nodeConfigs: Seq[Config] =
    NodeConfigs.newBuilder
      .withDefault(4)
      .buildNonConflicting()

}
