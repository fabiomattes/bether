package com.betherplatform.it.matcher

import com.betherplatform.account.PrivateKeyAccount
import com.betherplatform.it.Node
import com.betherplatform.transaction.assets.exchange.Order

sealed trait MatcherCommand extends Product with Serializable
object MatcherCommand {
  case class Place(node: Node, order: Order)                            extends MatcherCommand
  case class Cancel(node: Node, owner: PrivateKeyAccount, order: Order) extends MatcherCommand
}
