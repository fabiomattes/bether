package com.betherplatform.it.sync.smartcontract

import com.typesafe.config.{Config, ConfigFactory}
import com.betherplatform.account.AddressScheme
import com.betherplatform.common.state.ByteStr
import com.betherplatform.common.utils.EitherExt2
import com.betherplatform.it.api.SyncHttpApi._
import com.betherplatform.it.NodeConfigs.Default
import com.betherplatform.it.sync.{issueFee, minFee, setScriptFee, smartFee, smartMinFee}
import com.betherplatform.it.transactions.BaseTransactionSuite
import com.betherplatform.it.util._
import com.betherplatform.lang.v1.FunctionHeader
import com.betherplatform.lang.v1.compiler.Terms
import com.betherplatform.transaction.Asset
import com.betherplatform.transaction.Asset.Bether
import com.betherplatform.transaction.assets.{IssueTransactionV2, SetAssetScriptTransaction}
import com.betherplatform.transaction.smart.{InvokeScriptTransaction, SetScriptTransaction}
import com.betherplatform.transaction.smart.script.ScriptCompiler
import com.betherplatform.transaction.transfer.TransferTransactionV2
import org.scalatest.CancelAfterFailure
import play.api.libs.json.JsNumber

import scala.concurrent.duration._

class Ride4DAppsActivationTestSuite extends BaseTransactionSuite with CancelAfterFailure {
  import Ride4DAppsActivationTestSuite._

  override protected def nodeConfigs: Seq[Config] = configWithRide4DAppsFeature

  private val smartAcc  = pkByAddress(firstAddress)
  private val callerAcc = pkByAddress(secondAddress)

  val scriptV3 = ScriptCompiler.compile("""
                                            |{-# STDLIB_VERSION 3 #-}
                                            |{-# CONTENT_TYPE CONTRACT #-}
                                            |
                                            |@Callable(i)
                                            |func doAction() = { WriteSet([DataEntry("0", true)]) }
                                            |
                                            |@Verifier(i)
                                            |func verify() = { true }
                                          """.stripMargin).explicitGet()._1
  val scriptV2 = ScriptCompiler.compile("""
                                          |func isTrue() = true
                                          |isTrue()
                                        """.stripMargin).explicitGet()._1

  test("send bether to accounts") {
    val contractTransfer =
      TransferTransactionV2
        .selfSigned(
          assetId = Bether,
          sender = sender.privateKey,
          recipient = smartAcc,
          amount = 5.bether,
          timestamp = System.currentTimeMillis(),
          feeAssetId = Bether,
          feeAmount = minFee,
          attachment = Array.emptyByteArray
        )
        .explicitGet()
    val contractTransferId = sender
      .signedBroadcast(contractTransfer.json() + ("type" -> JsNumber(TransferTransactionV2.typeId.toInt)))
      .id

    val callerTransfer =
      TransferTransactionV2
        .selfSigned(
          assetId = Bether,
          sender = sender.privateKey,
          recipient = callerAcc,
          amount = 5.bether,
          timestamp = System.currentTimeMillis(),
          feeAssetId = Bether,
          feeAmount = minFee,
          attachment = Array.emptyByteArray
        )
        .explicitGet()
    val callerTransferId = sender
      .signedBroadcast(callerTransfer.json() + ("type" -> JsNumber(TransferTransactionV2.typeId.toInt)))
      .id

    nodes.waitForHeightAriseAndTxPresent(callerTransferId)
    nodes.waitForTransaction(contractTransferId)
  }

  test("can't set contract to account before Ride4DApps activation") {
    val setScriptTransaction = SetScriptTransaction
      .selfSigned(smartAcc, Some(scriptV3), setScriptFee + smartFee, System.currentTimeMillis())
      .explicitGet()
    assertBadRequestAndMessage(
      sender
        .signedBroadcast(setScriptTransaction.json() + ("type" -> JsNumber(SetScriptTransaction.typeId.toInt))),
      "RIDE 4 DAPPS feature has not been activated yet"
    )
  }

  test("can't set script with user function to account before Ride4DApps activation") {
    val setFuncScriptTransaction = SetScriptTransaction
      .selfSigned(smartAcc, Some(scriptV2), setScriptFee, System.currentTimeMillis())
      .explicitGet()
    assertBadRequestAndMessage(
      sender
        .signedBroadcast(setFuncScriptTransaction.json() + ("type" -> JsNumber(SetScriptTransaction.typeId.toInt))),
      "RIDE 4 DAPPS feature has not been activated yet"
    )
  }

  test("can't invoke script before Ride4DApps activation") {
    val invokeScriptTransaction = InvokeScriptTransaction
      .selfSigned(callerAcc,
                  smartAcc.toAddress,
                  Terms.FUNCTION_CALL(FunctionHeader.User("foo"), List.empty),
                  Seq.empty,
                  smartMinFee,
                  Bether,
                  System.currentTimeMillis())
      .explicitGet()
    assertBadRequestAndMessage(
      sender.signedBroadcast(invokeScriptTransaction.json() + ("type" -> JsNumber(InvokeScriptTransaction.typeId.toInt))),
      "RIDE 4 DAPPS feature has not been activated yet"
    )
  }

  test("can't issue asset with user function in script before Ride4DApps activation") {
    val issueTransaction = IssueTransactionV2
      .selfSigned(
        AddressScheme.current.chainId,
        smartAcc,
        "Test".getBytes,
        "Test asset".getBytes,
        1000,
        0,
        reissuable = true,
        Some(scriptV2),
        issueFee,
        System.currentTimeMillis()
      )
      .explicitGet()
    assertBadRequestAndMessage(
      sender.signedBroadcast(issueTransaction.json() + ("type" -> JsNumber(IssueTransactionV2.typeId.toInt))),
      "RIDE 4 DAPPS feature has not been activated yet"
    )
  }

  test("can't set script with user function to asset before Ride4DApps activation") {
    val setAssetScriptTransaction = SetAssetScriptTransaction
      .signed(AddressScheme.current.chainId,
              smartAcc,
              Asset.IssuedAsset("Test".getBytes),
              Some(scriptV2),
              issueFee,
              System.currentTimeMillis(),
              smartAcc)
      .explicitGet()
    assertBadRequestAndMessage(
      sender.signedBroadcast(setAssetScriptTransaction.json() + ("type" -> JsNumber(SetAssetScriptTransaction.typeId.toInt))),
      "RIDE 4 DAPPS feature has not been activated yet"
    )
  }

  test(s"wait height from to $activationHeight for feature activation") {
    sender.waitForHeight(activationHeight, 5.minutes)
  }

  test("can issue asset and set script with user function after Ride4DApps activation") {
    val issueTransaction = IssueTransactionV2
      .selfSigned(
        AddressScheme.current.chainId,
        smartAcc,
        "Test".getBytes,
        "Test asset".getBytes,
        1000,
        0,
        reissuable = true,
        Some(scriptV2),
        issueFee,
        System.currentTimeMillis()
      )
      .explicitGet()
    val issueTxId = sender.signedBroadcast(issueTransaction.json() + ("type" -> JsNumber(IssueTransactionV2.typeId.toInt))).id
    sender.waitForTransaction(issueTxId)

    val setAssetScriptTransaction = SetAssetScriptTransaction
      .signed(
        AddressScheme.current.chainId,
        smartAcc,
        Asset.IssuedAsset(ByteStr.decodeBase58(issueTxId).get),
        Some(scriptV2),
        issueFee,
        System.currentTimeMillis(),
        smartAcc
      )
      .explicitGet()
    val txId = sender.signedBroadcast(setAssetScriptTransaction.json() + ("type" -> JsNumber(SetAssetScriptTransaction.typeId.toInt))).id
    sender.waitForTransaction(txId)
  }

  test("can set contract and invoke script after Ride4DApps activation") {
    val setScriptTransaction = SetScriptTransaction
      .selfSigned(smartAcc, Some(scriptV3), setScriptFee + smartFee, System.currentTimeMillis())
      .explicitGet()
    val setScriptTxId = sender.signedBroadcast(setScriptTransaction.json() + ("type" -> JsNumber(SetScriptTransaction.typeId.toInt))).id
    sender.waitForTransaction(setScriptTxId)

    val invokeScriptTransaction = InvokeScriptTransaction
      .selfSigned(callerAcc,
                  smartAcc.toAddress,
                  Terms.FUNCTION_CALL(FunctionHeader.User("doAction"), List.empty),
                  Seq.empty,
                  smartMinFee,
                  Bether,
                  System.currentTimeMillis())
      .explicitGet()
    val invokeTxId = sender.signedBroadcast(invokeScriptTransaction.json() + ("type" -> JsNumber(InvokeScriptTransaction.typeId.toInt))).id
    sender.waitForTransaction(invokeTxId)

    val setFuncScriptTransaction = SetScriptTransaction
      .selfSigned(smartAcc, Some(scriptV2), setScriptFee + smartFee, System.currentTimeMillis())
      .explicitGet()
    val setFuncScriptTxId = sender.signedBroadcast(setFuncScriptTransaction.json() + ("type" -> JsNumber(SetScriptTransaction.typeId.toInt))).id
    sender.waitForTransaction(setFuncScriptTxId)
  }

  test("can add user function to account script after Ride4DApps activation") {
    val setScriptTransaction = SetScriptTransaction
      .selfSigned(smartAcc, Some(scriptV2), setScriptFee + smartFee, System.currentTimeMillis())
      .explicitGet()
    val txId = sender.signedBroadcast(setScriptTransaction.json() + ("type" -> JsNumber(SetScriptTransaction.typeId.toInt))).id
    sender.waitForTransaction(txId)
  }
}

object Ride4DAppsActivationTestSuite {
  val activationHeight = 25

  private val configWithRide4DAppsFeature: Seq[Config] =
    Default.map(ConfigFactory.parseString(s"""
                                             | bether.blockchain.custom.functionality {
                                             |   pre-activated-features.11 = ${activationHeight - 1}
                                             |}""".stripMargin).withFallback(_))

}
