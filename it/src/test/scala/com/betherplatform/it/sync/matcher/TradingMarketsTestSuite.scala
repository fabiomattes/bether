package com.betherplatform.it.sync.matcher

import com.typesafe.config.Config
import com.betherplatform.it.ReportingTestName
import com.betherplatform.it.api.SyncHttpApi._
import com.betherplatform.it.api.SyncMatcherHttpApi._
import com.betherplatform.it.sync._
import com.betherplatform.it.sync.matcher.config.MatcherPriceAssetConfig._
import com.betherplatform.it.transactions.NodesFromDocker
import com.betherplatform.transaction.assets.exchange.OrderType.BUY
import org.scalatest._

class TradingMarketsTestSuite
    extends FreeSpec
    with Matchers
    with BeforeAndAfterAll
    with CancelAfterFailure
    with NodesFromDocker
    with ReportingTestName {
  override protected def nodeConfigs: Seq[Config] = Configs

  private def matcher = dockerNodes().head
  private def alice   = dockerNodes()(1)

  val (amount, price) = (1000L, 1000000000L)

  val wctTxId = matcher.signedIssue(createSignedIssueRequest(IssueWctTx)).id
  matcher.waitForTransaction(wctTxId)

  "When some orders were placed and matcher was restarted" - {
    val order = matcher.placeOrder(alice.privateKey, wctBetherPair, BUY, amount, price, matcherFee).message.id
    matcher.waitOrderStatus(wctBetherPair, order, "Accepted")

    docker.restartNode(matcher)

    "Trading markets have info about all asset pairs" in {
      val markets = matcher.tradingMarkets().markets
      markets.size shouldBe 1
      markets.head.amountAssetName shouldNot be("Unknown")
      markets.head.priceAssetName shouldNot be("Unknown")
    }
  }
}
